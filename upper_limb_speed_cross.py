import numpy as np
from util.preprocess import resample
from util.megatrial_preprocess import low_pass
import os as sys
from models.mvtsgan import MVTSGAN

def read_imu_from_file(subject, trial, segment , DATA_ROOT):
    csvfile = f'{DATA_ROOT}/{subject}/IMU raw/{trial}/{segment}.csv'
    data = np.nan_to_num(np.loadtxt(csvfile, delimiter=',')[:, :-3])
    passed = low_pass(data)
    return passed

def read_mocap_from_file_with_scale_new(subject, trial, joint, DATA_ROOT):
    csvfile = f'{DATA_ROOT}/{subject}/Vicon results/{trial}_{joint}_IK.csv'
    data = np.loadtxt(csvfile, delimiter=',')#[:0]
    scale = data.max()-data.min()
    data -= data.min()
    data /= data.max()
    return data, scale


def read_mocap_from_file_with_scale_old(subject, trial, joint, DATA_ROOT):
    csvfile = f'{DATA_ROOT}/{subject}/Vicon results/upper/{trial}_{joint}_IK.csv'
    data = np.loadtxt(csvfile, delimiter=',')#[:0]
    scale = data.max()-data.min()
    data -= data.min()
    data /= data.max()
    return data, scale


def read_all_for_tasks_with_scale(subject_list, trials_list, segment_list, joint, data_root, old=True):
    all_mocaps = []
    all_subs = []
    for tr in trials_list:
        for s in subject_list:
            try:
                subjects_segments = []
                for l in segment_list:
                    subjects_segments.append(read_imu_from_file(s, tr, l, data_root))
                mocap_for_joint, scale = read_mocap_from_file_with_scale_old(s, tr, joint, data_root) if old \
                    else read_mocap_from_file_with_scale_new(s, tr, joint, data_root)

                subjects_segments = np.concatenate(subjects_segments, axis=1)
                subjects_segments, mocap_for_joint = resample(subjects_segments, mocap_for_joint)

                if subjects_segments.shape[0] != mocap_for_joint.shape[0]:
                    raise AssertionError(f'mismatch {mocap_for_joint.shape[0]}, {subjects_segments.shape[0]}')
                all_mocaps.append(mocap_for_joint)
                all_subs.append(subjects_segments)
            except FileNotFoundError:
                continue

    X = np.concatenate(all_subs, axis=0)
    Y = np.concatenate(all_mocaps, axis=0)

    if (X.shape[0] != Y.shape[0]):
        raise AssertionError("data sample mismatch")

    return X, Y, scale

JOINTS = [ 'elbow', 'humeral']


lr=0.000001
batch_len=350
epochs=500

def powerset(s):
    x = len(s)
    masks = [1 << i for i in range(x)]
    for i in range(1 << x):
        yield [ss for mask, ss in zip(masks, s) if i & mask]

male_sub = 'Male Subject {}'
female_sub = 'Female Subject {}'
upper = False
male_subs = map(lambda x: male_sub.format(x), range(1, 16))
female_subs = map(lambda x: female_sub.format(x), range(1, 16))

old_subjects = []
old_subjects.extend(list(male_subs))
old_subjects.extend(list(female_subs))

old_tasks_train = [['AB_F', 'FL_F', 'horiFL_F'], ['AB_S', 'FL_S', 'hori_FL_S']]
old_tasks_test = [['reach_S', 'reach_S2', 'head_S', 'head_S2'],['reach_F', 'reach_F2', 'head_F', 'head_F2']]

new_subjects = [f'subject {i}' for i in range(1, 6)]
segment_list = ['forearm', 'thorax', 'upperarm']
training_task_new = ['FF45', 'FF90', 'FH90', 'FS45', 'FS90', 'PF45', 'PF90', 'PH90', 'PS45', 'PS90', 'Rand']
testing_task_new = ['Ball', 'Wave']
sensor_combos = list(powerset(segment_list))[1:]
sensor_combos = sensor_combos[::-1]
print(sensor_combos)


DATA_SAVE_ROOT = '/data/gpfs/projects/punim1184/neuralkinematics/UPPERLIMB_SPEEDS'
old_root = '/data/gpfs/projects/punim1184/Zhou Fang data'
new_root = '/data/gpfs/projects/punim1184/Zhou Fang data/Cleaned elbow data'
experiment_counter = 0
''' Experiment Setting '''
for joint in JOINTS:
    print('*'*40)
    print(f'joint = {joint}')
    print('*'*40)
    for sci, sc in enumerate(sensor_combos):

        ''' Creating 5 LOO CV experiments for the new subjects'''

        for sp in range(2):


            new_train_subs = list(set(new_subjects) )
            training_task_old = old_tasks_train[sp]
            testing_task_old = old_tasks_test[sp]

            print(f'sc = {sc}')
            print(f'loading training tasks {training_task_old} for subjects {old_subjects}')
            X_tr_o, Y_tr_o, _ = read_all_for_tasks_with_scale(old_subjects, training_task_old, sc, joint, old_root)

            print(f'loading training tasks {training_task_new} for subjects {new_train_subs}')
            X_tr_n, Y_tr_n, _ = read_all_for_tasks_with_scale(new_train_subs, training_task_new, sc, joint, new_root,
                                                              old=False)

            X_tr = np.concatenate((X_tr_n, X_tr_o), axis=0)
            Y_tr = np.concatenate((Y_tr_n, Y_tr_o), axis=0)

            print(X_tr.shape)
            print(Y_tr.shape)
            '''Creating a Model'''
            chkpt_path = f'{DATA_SAVE_ROOT}/{joint}/{sci}/{sp}/'
            sys.makedirs(chkpt_path, exist_ok=True)
            model = MVTSGAN(X_tr.shape[1], output_streams=Y_tr.shape[1], epochs=epochs, lr=lr, batch_len=batch_len, saturation=False,
                      rand_noise=False,
                      verbose=1, chkpt_path=chkpt_path)

            model.fit(X_tr, Y_tr)
            # print(f'loading testing tasks {testing_task_old} for subjects {old_subjects}')
            for os in old_subjects:
                for ot in testing_task_old:
                    try:
                        X_te_o, Y_te_o, scale_o = read_all_for_tasks_with_scale([os], [ot], sc, joint, old_root)
                        print(f'testing for {ot} on {os}: {X_te_o.shape}, {Y_te_o.shape}')

                        Y_pred_o = model.generator.predict(
                    X_te_o.reshape((1, X_te_o.shape[0], X_te_o.shape[1]))).reshape((Y_te_o.shape))
                        folder_name = f'{DATA_SAVE_ROOT}/{joint}/{sci}/{sp}/old_{os}_{ot}'

                        sys.makedirs(folder_name, exist_ok = True)
                        np.savetxt(f'{folder_name}/target.csv', Y_te_o)
                        np.savetxt(f'{folder_name}/prediction.csv', Y_pred_o * scale_o)
                        print(folder_name)
                    except ValueError:
                        print(f'empty array for {ot}/{os}')


                # exit(0)
            experiment_counter += 1



print('*'*40)
print(f'total experiments = {experiment_counter}')
print('*'*40)