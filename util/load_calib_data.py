from util.megatrial_preprocess import low_pass
import numpy as  np


def read_imu_calib_file(segment, IMU_folder ):
    csvfile = f'{IMU_folder}/{segment}.csv'
    data = np.loadtxt(csvfile, delimiter=',')[:, :-3]
    passed = low_pass(data)
    return passed


def get_calibration_imus(seg_list, folder_list):

    concat_data = []

    for f in folder_list:
        sub_data = []
        for seg in seg_list:
            dat = read_imu_calib_file(seg, f)
            sub_data.append(dat)
        sub_data = np.concatenate(sub_data, axis=1)

        concat_data.append(sub_data)

    concat_data = np.concatenate(concat_data, axis=0)

    return concat_data