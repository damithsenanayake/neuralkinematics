import numpy as np
from scipy.signal import sosfilt, butter

rst = np.random.RandomState(seed=42)
def low_pass(X, rate = 1):
    out = []
    n_sets = len(X)//rate

    for i in range(n_sets):
        st = i * rate
        et = (i+1) * rate
        cand = X[st:min(et, len(X))].mean(axis=0)


        out.append(cand)
    out = np.array(out)
    res = []

    for i in range(X.shape[1]):

        cand = np.nan_to_num(out[:, i])
        if np.any(np.isnan(cand)):
            # print(cand)
            # print(len(np.where(np.isnan(cand))))
            # print(np.where(np.isnan(cand)))
            raise Exception('Nans found at the lowpass filtering')

        # print(f'DEBUGGER: cand.max() = {cand.max()}')
        cand -= cand.min()
        if not cand.max() == 0:
            # raise Exception("Denominator is zero")
            cand /= cand.max()

        sos = butter(4, 5, fs=100, btype='lowpass', output='sos')
        cand = sosfilt(sos, cand)

        res.append(cand)
    res = np.array(res).T

    res -= res.mean()
    # if res.std():
    res /= res.std()

    return res
def read_imu_from_file(subject, trial, segment , DATA_ROOT):
    csvfile = f'{DATA_ROOT}/{subject}/IMU raw/{trial}/{segment}.csv'
    data = np.nan_to_num(np.loadtxt(csvfile, delimiter=',')[:, :-3])
    passed = low_pass(data)
    return passed
def read_imu_upper_file(subject, trial, segment, DATA_ROOT ):
    csvfile = f'{DATA_ROOT}/{subject}/IMU raw/{trial}/{segment}.csv'
    data = np.nan_to_num(np.loadtxt(csvfile, delimiter=',')[:, :-3])
    passed = low_pass(data)
    return passed

def read_all_for_tasks(subject_list, trials_list, segment_list, joint, data_root):
    all_mocaps = []
    all_subs = []
    for tr in trials_list:
        for s in subject_list:
                subjects_segments = []
                for l in segment_list:
                    subjects_segments.append(read_imu_from_file(s, tr, l, data_root))
                mocap_for_joint = read_mocap_from_file(s, tr, joint, data_root)

                subjects_segments = np.concatenate(subjects_segments, axis=1)
                subjects_segments, mocap_for_joint = resample(subjects_segments, mocap_for_joint)
                all_mocaps.append(mocap_for_joint)
                all_subs.append(subjects_segments)

    return np.concatenate(all_subs, axis=0), np.concatenate(all_mocaps, axis=0)

def read_all_for_tasks_with_scale(subject_list, trials_list, segment_list, joint, data_root):
    all_mocaps = []
    all_subs = []
    for tr in trials_list:
        for s in subject_list:
                subjects_segments = []
                for l in segment_list:
                    subjects_segments.append(read_imu_from_file(s, tr, l, data_root))
                mocap_for_joint, scale = read_mocap_from_file_with_scale(s, tr, joint, data_root)

                subjects_segments = np.concatenate(subjects_segments, axis=1)
                subjects_segments, mocap_for_joint = resample(subjects_segments, mocap_for_joint)
                all_mocaps.append(mocap_for_joint)
                all_subs.append(subjects_segments)

    return np.concatenate(all_subs, axis=0), np.concatenate(all_mocaps, axis=0), scale

def read_all_for_tasks_upper(subject_list, trials_list, segment_list, joint, data_root):
    all_mocaps = []
    all_subs = []
    for tr in trials_list:
        for s in subject_list:
            skip_sub_trial = False

            subjects_segments = []
            for l in segment_list:
                try:
                    subjects_segments.append(read_imu_from_file(s, tr, l, data_root))
                except Exception:
                    skip_sub_trial = True
                    break
            try:
                mocap_for_joint, loc, scale = read_upper_mocap(s, tr, joint, data_root)
            except Exception:
                skip_sub_trial = True
                break
            if not skip_sub_trial:
                subjects_segments = np.concatenate(subjects_segments, axis=1)
                subjects_segments, mocap_for_joint = resample(subjects_segments, mocap_for_joint)
                all_mocaps.append(mocap_for_joint)
                all_subs.append(subjects_segments)

    return np.concatenate(all_subs, axis=0), np.concatenate(all_mocaps, axis=0), loc, scale
def read_mocap_from_file(subject, trial, joint, DATA_ROOT):
    csvfile = f'{DATA_ROOT}/{subject}/Vicon results/gait/{trial}_{joint}_IK.csv'
    data = np.loadtxt(csvfile, delimiter=',')#[:0]
    data -= data.min()
    data /= data.max()
    return data
def read_mocap_from_file_with_scale(subject, trial, joint, DATA_ROOT):
    csvfile = f'{DATA_ROOT}/{subject}/Vicon results/gait/{trial}_{joint}_IK.csv'
    data = np.loadtxt(csvfile, delimiter=',')#[:0]
    scale = data.max()-data.min()
    data -= data.min()
    data /= data.max()
    return data, scale

def resample(IMU, VIC):
    if len(IMU) > len(VIC):
        target = VIC.shape[0]
        perm = rst.permutation(IMU.shape[0])
        perm = perm[:target]
        ixs = np.sort(perm)
        IMU = IMU[ixs]
    elif len(IMU) < len(VIC):
        target = IMU.shape[0]
        perm = rst.permutation(VIC.shape[0])
        perm = perm[:target]
        ixs = np.sort(perm)
        VIC = VIC[ixs]

    return IMU, VIC

def read_all_data_for_motion(subject_list, trial, segment_list, angle, data_root):

    all_subs = []
    all_mocaps = []
    for s in subject_list:

        subjects_segments = []
        for l in segment_list:
            subjects_segments.append(read_imu_from_file(s, trial, l, data_root))
        mocap_for_joint = read_mocap_from_file(s, trial, angle, data_root)

        subjects_segments = np.concatenate(subjects_segments, axis=1)
        subjects_segments, mocap_for_joint = resample(subjects_segments, mocap_for_joint)
        all_mocaps.append(mocap_for_joint)
        all_subs.append(subjects_segments)

    return np.concatenate(all_subs, axis=0), np.concatenate(all_mocaps, axis=0)
#
# def read_upper_mocap(subject, trial, joint, DATA_ROOT):
#     csvfile = f'{DATA_ROOT}/{subject}/Vicon results/upper/{trial}_{joint}_IK.csv'
#     data = np.loadtxt(csvfile, delimiter=',')#[: 1000]
#     loc = data.min()
#     scale = data.max()
#     data -= data.min()
#     data /= data.max()
#     data -= data.mean()
#     return data, loc, scale
# def read_all_data_for_motion_upper(subject_list, trial, segment_list, angle, data_root):
#
#     all_subs = []
#     all_mocaps = []
#     locs = []
#     scales =[]
#     for s in subject_list:
#
#         subjects_segments = []
#         for l in segment_list:
#             subjects_segments.append(read_imu_from_file(s, trial, l, data_root))
#         mocap_for_joint, loc, scale = read_upper_mocap(s, trial, angle, data_root)
#         locs.append(loc)
#         scales.append(scale)
#         subjects_segments = np.concatenate(subjects_segments, axis=1)
#         subjects_segments, mocap_for_joint = resample(subjects_segments, mocap_for_joint)
#         all_mocaps.append(mocap_for_joint)
#         all_subs.append(subjects_segments)
#
#     return np.concatenate(all_subs, axis=0), np.concatenate(all_mocaps, axis=0), np.mean(loc), np.mean(scale)
