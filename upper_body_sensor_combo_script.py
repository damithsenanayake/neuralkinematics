import os

import numpy as np
from util.preprocess import resample
# from util.megatrial_preprocess import low_pass
import matplotlib.pyplot as plt
import os as sys
from models.mvtsgan_batch import MVTSGAN
from scipy.signal import sosfilt, butter

def low_pass(X, rate = 1):


    res = []

    for i in range(X.shape[1]):

        cand = np.nan_to_num(X[:, i])
        if np.any(np.isnan(cand)):
            # print(cand)
            # print(len(np.where(np.isnan(cand))))
            # print(np.where(np.isnan(cand)))
            raise Exception('Nans found at the lowpass filtering')

        # print(f'DEBUGGER: cand.max() = {cand.max()}')
        cand -= cand.min()
        if not cand.max() == 0:
            # raise Exception("Denominator is zero")
            cand /= cand.max()

        sos = butter(20, 10, fs=100, btype='lowpass', output='sos')
        cand = sosfilt(sos, cand)

        res.append(cand)
    res = np.array(res).T

    res -= res.mean()
    # if res.std():
    res /= res.std()

    return res
def read_imu_from_file(subject, trial, segment , DATA_ROOT):
    csvfile = f'{DATA_ROOT}/{subject}/IMU raw/{trial}/{segment}.csv'
    
    data = np.nan_to_num(np.loadtxt(csvfile, delimiter=',')[:, :-3])
    passed = low_pass(data)
    return passed

def read_imu_from_file_with_calibration(subject, trial, segment , DATA_ROOT):
    csvfile = f'{DATA_ROOT}/{subject}/IMU raw/{trial}/{segment}.csv'
    data = np.nan_to_num(np.loadtxt(csvfile, delimiter=',')[:, :-3])
    passed = low_pass(data)

    
    return passed

def read_mocap_from_file_with_scale_new(subject, trial, joint, DATA_ROOT):
    csvfile = f'{DATA_ROOT}/{subject}/Vicon results/{trial}_{joint}_IK.csv'
    data = np.loadtxt(csvfile, delimiter=',')[:]
    # print(f'func: read_mocap_from_file_with_scale_new: len = {len(data)}')

    scale = data.std()#data.max()-data.min()
    # data -= data.mean(axis=0)
    # data /= scale
    return data, scale


def read_mocap_from_file_with_scale_old(subject, trial, joint, DATA_ROOT):
    csvfile = f'{DATA_ROOT}/{subject}/Vicon results/upper/{trial}_{joint}_IK.csv'
    data = np.loadtxt(csvfile, delimiter=',')[:]
    # print(f'func: read_mocap_from_file_with_scale_old: len = {len(data)}')
    scale = data.std()  # data.max()-data.min()
    # data -= data.mean(axis=0)
    # data /= scale
    return data, scale


def read_all_for_tasks_with_scale(subject_list, trials_list, segment_list, joint, data_root, old=True):
    all_mocaps = []
    all_subs = []
    for tr in trials_list:
        for s in subject_list:
            try:
                subjects_segments = []
                # print(f'DEBUG: SUB = {s}')
                for l in segment_list:
                    subjects_segments.append(read_imu_from_file(s, tr, l, data_root))
                mocap_for_joint, _ = read_mocap_from_file_with_scale_old(s, tr, joint, data_root) if old \
                    else read_mocap_from_file_with_scale_new(s, tr, joint, data_root)
                # print(f'LEN(MOCAP) = {len(mocap_for_joint)}')
                subjects_segments = np.concatenate(subjects_segments, axis=1)
                subjects_segments, mocap_for_joint = resample(subjects_segments, mocap_for_joint)
                # print(f'LEN(MOCAP) after resample = {len(mocap_for_joint)}')
                if subjects_segments.shape[0] != mocap_for_joint.shape[0]:
                    raise AssertionError(f'mismatch {mocap_for_joint.shape[0]}, {subjects_segments.shape[0]}')
                all_mocaps.append(mocap_for_joint)
                all_subs.append(subjects_segments)
            except FileNotFoundError:
                continue

    X = np.concatenate(all_subs, axis=0)

    Y = np.concatenate(all_mocaps, axis=0)
    scale = Y.std()

    Y -= Y.mean(axis=0)
    Y /= scale
    if (X.shape[0] != Y.shape[0]):
        raise AssertionError("data sample mismatch")

    return X, Y, scale

JOINTS = ['humeral', 'elbow']


lr=0.0000001
batch_len=150
epochs=500

def powerset(s):
    x = len(s)
    masks = [1 << i for i in range(x)]
    for i in range(1 << x):
        yield [ss for mask, ss in zip(masks, s) if i & mask]

male_sub = 'Male Subject {}'
female_sub = 'Female Subject {}'
upper = False
male_subs = map(lambda x: male_sub.format(x), range(1, 16))
female_subs = map(lambda x: female_sub.format(x), range(1, 16))

old_subjects = []
old_subjects.extend(list(male_subs))
old_subjects.extend(list(female_subs))

new_subjects = [f'subject {i}' for i in range(1, 6)]
segment_list = ['forearm', 'thorax', 'upperarm']


training_task_old = {'humeral': ['reach_F', 'reach_S', 'head_F', 'head_S', 'AB_F', 'AB_S', 'FL_F', 'FL_S', 'horiFL_F', 'horiFL_S'] , 'elbow': ['FL_F', 'AB_F']}#['AB_F', 'AB_S', 'FL_F', 'FL_S', 'horiFL_F', 'horiFL_S']
training_task_new = {'elbow':['FF45', 'FF90', 'FH90', 'FS45', 'FS90', 'PF45', 'PF90', 'PH90', 'PS45', 'PS90'], 'humeral': ['Rand', 'FF45', 'FF90', 'FH90', 'FS45', 'FS90', 'PF45', 'PF90', 'PH90', 'PS45', 'PS90']}
testing_task_old = [ 'reach_F2']#, 'reach_S', 'reach_S2', 'head_F', 'head_F2', 'head_S', 'head_S2']
testing_task_new = ['Ball', 'Wave']
sensor_combos = list(powerset(segment_list))[1:]
sensor_combos = sensor_combos[::-1]
print(sensor_combos)


DATA_SAVE_ROOT = '/data/gpfs/projects/punim1184/neuralkinematics/UPPERLIMB_FINAL'
old_root = '/data/gpfs/projects/punim1184/Zhou Fang data'
new_root = '/data/gpfs/projects/punim1184/Zhou Fang data/Cleaned elbow data'
experiment_counter = 0
''' Experiment Setting '''
for joint in JOINTS:
    print('*'*40)
    print(f'joint = {joint}')
    print('*'*40)
    for sci, sc in enumerate(sensor_combos):

        ''' Creating 5 LOO CV experiments for the new subjects'''

        for lo in range(1):

            left_out = new_subjects[lo]
            new_train_subs = list(set(new_subjects) - set([left_out]))

            print(f'sc = {sc}')
            print(f'loading training tasks {training_task_old[joint]} for subjects {old_subjects}')
            X_tr_o, Y_tr_o, _ = read_all_for_tasks_with_scale(old_subjects, training_task_old[joint], sc, joint, old_root)
            print(X_tr_o.shape)
            print(f'loading training tasks {training_task_new[joint]} for subjects {new_train_subs}')
            X_tr_n, Y_tr_n, tr_scale = read_all_for_tasks_with_scale(new_train_subs, training_task_new[joint], sc, joint, new_root,
                                                              old=False)

            figure, axes = plt.subplots(Y_tr_n.shape[1], sharey=True)
            # print(f'data loaded for plotting {target.shape}')
            for i in range(Y_tr_n.shape[1]):
                ax = axes.flatten()[i]
                ax.plot(Y_tr_n.T[i], label='true')
                # ax.plot(Y_pred_n.T[i][200:800], label='predicted')
                ax.legend()
            os.makedirs(f'{DATA_SAVE_ROOT}/{joint}/{sci}/{lo}', exist_ok=True)
            plt.savefig(f'{DATA_SAVE_ROOT}/{joint}/{sci}/{lo}/training_data_target.png')
            #X_tr_n, Y_tr_n, tr_scale = read_all_for_tasks_with_scale([left_out], training_task_new[joint], sc, joint,
                                                                     # new_root,
                                                                     # old=False)
            print(X_tr_n.shape)


            X_tr = np.concatenate((X_tr_n, X_tr_o), axis=0) #if joint == 'elbow' else X_tr_o
            Y_tr = np.concatenate((Y_tr_n, Y_tr_o), axis=0) #if joint == 'elbow' else Y_tr_o

            print(X_tr.shape)
            print(Y_tr.shape)
            '''Creating a Model'''
            chkpt_path = f'{DATA_SAVE_ROOT}/{joint}/{sci}/{lo}/'
            sys.makedirs(chkpt_path, exist_ok=True)
            model = MVTSGAN(X_tr.shape[1], output_streams=Y_tr.shape[1], epochs=epochs, lr=lr, batch_len=batch_len, saturation=False,
                      rand_noise=False,
                      verbose=1, chkpt_path=chkpt_path)
            #
            model.fit(X_tr, Y_tr, pick_rate = 0.1)#0.2 if joint == 'humeral' else 1.0)
            print(f'loading testing tasks {testing_task_old} for subjects {old_subjects}')
            # for os in old_subjects[:2]:
            #     for ot in testing_task_old:
            #         try:
            #             X_te_o, Y_te_o, scale_o = read_all_for_tasks_with_scale([os], [ot], sc, joint, old_root)
            #             print(f'testing for {ot} on {os}: {X_te_o.shape}, {Y_te_o.shape}')
            #
            #             Y_pred_o = model.generator.predict(
            #         X_te_o.reshape((1, X_te_o.shape[0], X_te_o.shape[1]))).reshape((Y_te_o.shape))
            #             folder_name = f'{DATA_SAVE_ROOT}/{joint}/{sci}/{lo}/old_{os}_{ot}'
            #
            #             sys.makedirs(folder_name, exist_ok = True)
            #             np.savetxt(f'{folder_name}/target.csv', Y_te_o * scale_o)
            #             np.savetxt(f'{folder_name}/prediction.csv', Y_pred_o * scale_o)
            #             print(folder_name)
            #             figure, axes = plt.subplots(Y_pred_o.shape[1], sharey=True)
            #             # print(f'data loaded for plotting {target.shape}')
            #             for i in range(Y_pred_o.shape[1]):
            #                 ax = axes.flatten()[i]
            #                 ax.plot(Y_te_o.T[i][200:800] * scale_o, label='true')
            #                 ax.plot(Y_pred_o.T[i][200:800] * scale_o, label='predicted')
            #                 ax.legend()
            #
            #             plt.savefig(f'{folder_name}/vis.png')
            #         except ValueError:
            #             print(f'empty array for {ot}/{os}')

            print(f'loading testing tasks {testing_task_new} for subjects {left_out}')
            for nt in testing_task_new:
                X_te_n, Y_te_n, scale_n = read_all_for_tasks_with_scale([left_out], [nt], sc, joint, new_root, old = False)
                print(f'testing for {nt} on {left_out}')
                Y_pred_n = model.generator.predict(
                    X_te_n.reshape((1, X_te_n.shape[0], X_te_n.shape[1]))).reshape((Y_te_n.shape))
                folder_name = f'{DATA_SAVE_ROOT}/{joint}/{sci}/{lo}/new_{left_out}_{nt}'

                # print(f'DEBUG: out script len Y_TE = {len(Y_te_n)}')
                sys.makedirs(folder_name, exist_ok=True)
                np.savetxt(f'{folder_name}/target.csv', Y_te_n )
                np.savetxt(f'{folder_name}/prediction.csv', Y_pred_n  )
                figure, axes = plt.subplots(Y_pred_n.shape[1], sharey=True)
                # print(f'data loaded for plotting {target.shape}')
                for i in range(Y_pred_n.shape[1]):
                    ax = axes.flatten()[i]
                    ax.plot(Y_te_n.T[i][200:800] , label='true')
                    ax.plot(Y_pred_n.T[i][200:800] , label='predicted')
                    ax.legend()

                plt.savefig(f'{folder_name}/vis.png')

                print(folder_name)
                print(folder_name)
                # exit(0)
            experiment_counter += 1
        break


print('*'*40)
print(f'total experiments = {experiment_counter}')
print('*'*40)