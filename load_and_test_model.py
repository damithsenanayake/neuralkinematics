import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import json
from util.megatrial_preprocess import read_all_for_tasks

male_sub = 'Male Subject {}'
female_sub = 'Female Subject {}'
upper = False
male_subs = map(lambda x: male_sub.format(x), range(1, 16))
female_subs = map(lambda x: female_sub.format(x), range(1, 16))
joint = 'knee'
segment_list = [ 'thigh', 'foot', 'shank']
task_list = [ 'gait30', 'gait50']
sub_list = []
sub_list.extend(list(male_subs))
sub_list.extend(list(female_subs))


model_string = json.dumps(json.load(open('/data/gpfs/projects/punim1184/neuralkinematics/mega_trial_checkpoints/knee/gen_knee.json', 'r')))

model = tf.keras.models.model_from_json(model_string)
model.load_weights('/data/gpfs/projects/punim1184/neuralkinematics/mega_trial_checkpoints/knee/gen_knee.h5')

print(model.summary())

for sub in sub_list:
    for task in task_list:
        try:
            X_test, Y_test = read_all_for_tasks([sub], [task], segment_list, joint)
        except:
            continue
        Y_pred = model.predict(X_test.reshape((1, X_test.shape[0], X_test.shape[1]))).reshape((Y_test.shape))
        fig, axes = plt.subplots(Y_test.shape[1], sharey=True)

        for i in range(Y_test.shape[1]):
            ax = axes.flatten()[i]
            ax.plot(Y_test.T[i][:400], label='true')
            ax.plot(Y_pred.T[i][:400], label='predicted')
            ax.legend()
        plt.savefig(f'results/png/{sub}_{task}.png')
        np.savetxt(f'results/csv/{sub}_{task}.csv',Y_pred, delimiter=',')