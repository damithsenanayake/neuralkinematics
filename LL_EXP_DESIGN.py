import numpy as np

from  LOWER_BODY_MASTER import LowerLimbExperiment
from util.preprocess import  resample
import matplotlib.pyplot as plt
from models.mvtsgan import MVTSGAN
from scipy.signal import butter,sosfilt
from util.visualizer import vis_imu
from util.megatrial_preprocess import read_all_for_tasks
import os
import json
import tensorflow as tf

male_sub = 'Male Subject {}'
female_sub = 'Female Subject {}'
upper = False
male_subs = map(lambda x: male_sub.format(x), range(1, 16))
female_subs = map(lambda x: female_sub.format(x), range(1, 16))
joint = 'ankle'
segment_list = ['shank', 'foot']
task_list = ['gait05', 'gait10']
test_tasks = ['gait15']
sub_list = []
sub_list.extend(list(male_subs))
sub_list.extend(list(female_subs))

sub_string = "{} Subject {}"

''' Experiment Setting '''
for tt in test_tasks:
    for i in range(1, 16):
        for s in ['Male', 'Female']:
            test_sub = sub_string.format(s, str(i))
            save_path = "CompoundExperiments/{}_{}".format(test_sub.replace(" ", "_"), str(tt))

            train_subs = np.setdiff1d( sub_list, [test_sub])
            '''joint, all_subs, all_tasks, test_tasks, train_subs, train_tasks, segments, save_path'''
            experiment = LowerLimbExperiment(joint, sub_list, task_list, [tt], train_subs, task_list, segment_list, save_path )
            experiment.initiate()
            experiment.run_experiment()
            experiment.save_results()


