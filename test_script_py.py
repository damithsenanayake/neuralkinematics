import numpy as np
from  LOWER_BODY_MASTER import LowerLimbExperiment
JOINTS = [ 'ankle', 'knee', 'hip']
def powerset(s):
    x = len(s)
    masks = [1 << i for i in range(x)]
    for i in range(1 << x):
        yield [ss for mask, ss in zip(masks, s) if i & mask]

segment_list = ['pelvis', 'thigh', 'shank', 'foot']
# test_tasks = ['gait05']
sensor_combos = list(powerset(segment_list))
print(sensor_combos)