!/bin/bash

#PBS -l ncpus=48
#PBS -l ngpus=4
#PBS -l mem=190GB
#PBS -l jobfs=200GB
#PBS -q normal
#PBS -P ph79
#PBS -l walltime=02:00:00
#PBS -l storage=gdata/ph79+scratch/ph79
#PBS -l wd

module load python3/3.9.2 tensorflow/2.6.0
python3 ~/neuralkinematics/load_test_upper.py > ~/neuralkinematics/testing_loading.out
