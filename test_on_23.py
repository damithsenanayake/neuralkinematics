import numpy as np
import pandas as pd

from LOWER_BODY_MASTER import LowerLimbExperiment
from util.megatrial_preprocess import read_all_for_tasks_with_scale
from models.mvtsgan import MVTSGAN
import os

JOINTS = ['ankle', 'knee', 'hip']


def powerset(s):
    x = len(s)
    masks = [1 << i for i in range(x)]
    for i in range(1 << x):
        yield [ss for mask, ss in zip(masks, s) if i & mask]


male_sub = 'Male Subject {}'
female_sub = 'Female Subject {}'
upper = False
male_subs = map(lambda x: male_sub.format(x), range(1, 16))
female_subs = map(lambda x: female_sub.format(x), range(1, 16))
# joint = 'hip'
segment_list = ['pelvis', 'thigh', 'shank', 'foot']
task_list = ['gait05', 'gait10', 'gait15']
# test_tasks = ['gait05']
sensor_combos = list(powerset(segment_list))[1:]
print(sensor_combos)

#
sub_list = []
sub_list.extend(list(male_subs))
sub_list.extend(list(female_subs))
''' ALL SUBJECTS LIST CREATED'''

''' CROSS_VALIDATION'''
OUTPUT_FOLDER = "split_corrected"
split = 25
rst = np.random.RandomState(seed=10)
training_subjects = rst.choice(sub_list, 25, replace=False)
test_subjects = np.setdiff1d(sub_list, training_subjects)
print(test_subjects)
config_names = []
''' Experiment Setting '''

configs = pd.read_csv('all_configs.csv')

sensor_combos = configs.segments.values

for joint in JOINTS:
    counter = 0

    for sci, sc in enumerate(sensor_combos):

        segments = sc.split(',')

        combo_path = f"sensor_combo_{sci}"

        for speed_combo_ix in range(6):
            print("*" * 25)
            print(f'expid = {sci} : sensor combo = {sc}')
            print("*" * 25)

            training_tasks = task_list

            test_tasks = ['gait20', 'gait30']

            config_name = str(counter)
            save_path = "{}/lower_limb_reporting_{}_{}/exp_{}".format(OUTPUT_FOLDER, combo_path, joint,
                                                                      config_name)

            print(save_path)# .format(test_sub.replace(" ", "_"), str(tt))
            '''joint, test_tasks, train_subs, test_subs, train_tasks, segments, save_path'''
            experiment = LowerLimbExperiment(joint, test_tasks, training_subjects, test_subjects, training_tasks, sc,
                                             save_path, config_name)

            for test_sub in test_subjects:
                for test_task in test_tasks:

                    X_te, Y_te, scale = read_all_for_tasks_with_scale([test_sub], [test_task], segments,joint,
                                                                      "/data/gpfs/projects/punim1193/ALEX_DATA")
                    checkpoint_path = f'{save_path}/chkpt'
                    model = MVTSGAN(X_te.shape[1], output_streams=Y_te.shape[1], chkpt_path=checkpoint_path)
                    Y_pred = model.generator.predict(
                        X_te.reshape((1, X_te.shape[0], X_te.shape[1]))).reshape((Y_te.shape))
                    os.makedirs("{}/{}_{}_{}".format(save_path, joint, test_sub, test_task), exist_ok=True)
                    np.savetxt("{}/{}_{}_{}/{}.csv".format(save_path, joint, test_sub, test_task, "target"),
                               Y_te * scale)
                    np.savetxt(
                        "{}/{}_{}_{}/{}.csv".format(save_path, joint, test_sub, test_task, "prediction"),
                        Y_pred * scale)
                    print(f'Subject: {test_sub}, Joint: {joint}, Sensors: {segments}, SPCOMBO: {counter}')
                    print(f'pred shape and target shape = {Y_pred.shape} {Y_te.shape}')

                print('Individual Results Saved')

            counter += 1

