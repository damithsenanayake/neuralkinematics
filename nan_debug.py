import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import json
from util.megatrial_preprocess import read_all_for_tasks_upper

male_sub = 'Male Subject {}'
female_sub = 'Female Subject {}'
upper = False
male_subs = map(lambda x: male_sub.format(x), range(15, 16))
female_subs = map(lambda x: female_sub.format(x), range(15, 16))
joint = 'elbow'
segment_list = ['forearm', 'upperarm', 'thorax']
task_list = ['AB_F', 'AB_S', 'FL_S', 'FL_F', 'head_F', 'head_S', 'reach_F', 'reach_S', 'horiFL_F', 'horiFL_S']
sub_list = []
sub_list.extend(list(male_subs))
sub_list.extend(list(female_subs))

# X, Y = read_all_for_tasks_upper(sub_list, task_list, segment_list, joint)


model_string = json.dumps(json.load(open('checkpoints/gen_elbow.json', 'r')))

model = tf.keras.models.model_from_json(model_string)
model.load_weights('checkpoints/gen_elbow.h5')

print(model.summary())

for sub in sub_list:
    for task in task_list:
        # try:
        X_test, Y_test = read_all_for_tasks_upper([sub], [task], segment_list, joint)
        # except:
        #     continue
        Y_pred = model.predict(X_test.reshape((1, X_test.shape[0], X_test.shape[1]))).reshape((Y_test.shape))
        fig, axes = plt.subplots(Y_test.shape[1], sharey=True)
        print(Y_pred)

        for i in range(Y_test.shape[1]):
            ax = axes.flatten()[i]
            ax.plot(Y_test.T[i][:400], label='true')
            ax.plot(Y_pred.T[i][:400], label='predicted')
            ax.legend()
        plt.savefig(f'results/png/{sub}_{task}.png')
        np.savetxt(f'results/csv/{sub}_{task}.csv',Y_pred, delimiter=',')