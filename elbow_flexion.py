import pandas as pd
import numpy as np
from util.preprocess import  resample
import matplotlib.pyplot as plt
from models.mvtsgan import MVTSGAN
from scipy.signal import butter,sosfilt
from util.visualizer import vis_imu
from util.megatrial_preprocess import read_all_data_for_motion_upper
import os
import tensorflow as tf

male_sub = 'Male Subject {}'
female_sub = 'Female Subject {}'

male_subs = map(lambda x: male_sub.format(x), range(1, 3))
female_subs = map(lambda x: female_sub.format(x), range(1, 3))

sub_list = []
sub_list.extend(list(male_subs))
sub_list.extend(list(female_subs))

X, Y = read_all_data_for_motion_upper(sub_list, 'head_F', ['forearm', 'upperarm'], 'elbow')
print(X.shape, Y.shape)
checkpoint_path = '/data/gpfs/projects/punim1184/neuralkinematics/mega_trial_checkpoints/elbow'
checkpoint_dir = os.path.dirname(checkpoint_path)
cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath = checkpoint_path, save_weights_only = True, verbose = 1)


# IMUF = np.zeros(IMU.shape)
# for v in range(IMU.shape[0]):
#     for l in range(IMU.shape[1]):
#         try:
#             IMUF[v, l] = float(IMU[v, l])
#         except ValueError:
#             print(v,',', l, ',' , IMU[v,l])

# print(IMU.shape)
# print(VIC.shape)
#
# print(IMU[:5])
# print(VIC[:5])

tr_rat = 0.8
#
#
# X, Y = resample(IMU, VIC)
# X -= X.mean(axis=0)
# Y -= Y.mean(axis=0)
tr_sz = int(X.shape[0] * tr_rat)
X_tr = X[:tr_sz]
Y_tr = Y[:tr_sz]
X_te = X[tr_sz:]
Y_te = Y[tr_sz:]
#
# print(X.shape)
# print(Y.shape)
#
fig, axes = plt.subplots(Y.shape[1], sharey=True)
net = MVTSGAN(X.shape[1], output_streams=Y.shape[1], epochs=2000, lr=0.00001, batch_len=500, saturation=False, rand_noise=False,
                  verbose=1, save_path = checkpoint_dir)
#
net.fit(X_tr, Y_tr)
#
Y_pr = net.generator.predict(X_te.reshape((1, X_te.shape[0], X_te.shape[1]))).reshape((Y_te.shape))
#
for i in range(Y.shape[1]):
    ax = axes.flatten()[i]
    ax.plot(Y_te.T[i][:400])
    ax.plot(Y_pr.T[i][:400])

gen_json = net.generator.to_json()
dis_json = net.discriminator.to_json()

with open(f"{checkpoint_path}/gen_elbow_head.json", "w") as json_file:
    json_file.write(gen_json)
with open(f"{checkpoint_path}/dis_elbow_head.json", "w") as json_file:
    json_file.write(dis_json)
# serialize weights to HDF5
net.generator.save_weights(f"{checkpoint_path}/gen_elbow_head.h5")
net.discriminator.save_weights(f"{checkpoint_path}/dis_elbow_head.h5")
# net.generator.save_weights(f'{checkpoint_dir}/generator.h5')
# net.discriminator.save_weights(f'{checkpoint_dir}/discriminator.h5')
plt.savefig(f'results/png/megatrial_el_head.png')
# plt.show()
# plt.close()
