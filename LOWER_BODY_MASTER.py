import numpy as np
import os
import matplotlib.pyplot as plt
from models.mvtsgan import MVTSGAN
from util.megatrial_preprocess import read_all_for_tasks, read_all_for_tasks_with_scale
import json
import os

class Experiment(object):
    def __int__(self, joint, all_subs, all_tasks, test_tasks, train_subs, train_tasks, segments, save_path):
        self.train_subs = train_subs
        self.train_tasks = train_tasks
        self.test_subs = np.setdiff1d(all_subs, train_subs)
        self.test_tasks = test_tasks#np.setdiff1d(all_tasks, train_tasks)
        self.segments = segments
        self.joint = joint
        self.save_path = save_path
        self.model = None

        config_file = open('./config.json', 'rb')

        self.configs = json.load(config_file)

    def load_data(self):

        pass

    def run_exp(self):

        pass


class LowerLimbExperiment(object):
    def __init__(self, joint, all_subs, all_tasks, test_tasks, train_subs, train_tasks, segments, save_path):
        # super(LowerLimbExperiment, self).__int__(joint, all_subs, all_tasks, test_tasks, train_subs, train_tasks, segments, save_path)
        self.train_subs = train_subs
        self.train_tasks = train_tasks
        self.test_subs = np.setdiff1d(all_subs, train_subs)
        self.test_tasks = test_tasks#np.setdiff1d(all_tasks, train_tasks)
        self.segments = segments
        self.joint = joint
        self.save_path = save_path
        self.model = None
        self.chkpt_path = os.path.join(save_path, 'chkpts')
        os.makedirs(self.chkpt_path, exist_ok=True)
        config_file = open('./config.json', 'rb')

        self.configs = json.load(config_file)

    def load_data(self):
        ''' Returns the training and testing data for the training and testing data configs '''
        print(f'Data Root = {self.configs["data_root"]}')
        print(f"subjects = {self.test_subs}")
        print(f"tasks = {self.train_tasks}")
        X_tr, Y_tr = read_all_for_tasks(self.train_subs, self.train_tasks, self.segments, self.joint, self.configs['data_root'])

        X_te, Y_te, scale = read_all_for_tasks_with_scale(self.test_subs, self.test_tasks, self.segments, self.joint, self.configs['data_root'])

        return X_tr, Y_tr, X_te, Y_te, scale

    def initiate(self, epochs = 2500, lr = 0.000001, batch_len = 250):

        ''' Load Data'''

        X_tr, Y_tr, X_te, Y_te, scale = self.load_data()

        self.scale = scale
        self.model = MVTSGAN(X_tr.shape[1], output_streams=Y_tr.shape[1], epochs=epochs, lr=lr, batch_len=batch_len, saturation=False,
                      rand_noise=False,
                      verbose=1, chkpt_path=self.chkpt_path)

        if X_tr.shape[1]!= X_te.shape[1]:
            raise AssertionError('Test and training data input dimensionality mismatch')

        if Y_tr.shape[1] != Y_te.shape[1]:
            raise AssertionError('Test and training data output dimensionality mismatch')

        self.X_tr, self.X_te, self.Y_tr, self.Y_te = X_tr, X_te, Y_tr, Y_te
        print('Data loaded and model initiated for running experiment')

    def run_experiment(self):

        ''' Train Model'''

        self.model.fit(self.X_tr, self.Y_tr)

        self.Y_pred = self.model.generator.predict(self.X_te.reshape((1, self.X_te.shape[0], self.X_te.shape[1]))).reshape((self.Y_te.shape))

    def save_individual_results(self):

        for test_sub in self.test_subs:
            for test_task in self.test_tasks:
                X_te, Y_te, scale = read_all_for_tasks_with_scale([test_sub], [test_task], self.segments,
                                                              self.joint, self.configs['data_root'])
                Y_pred = self.model.generator.predict(
                    X_te.reshape((1, X_te.shape[0], X_te.shape[1]))).reshape((Y_te.shape))
                os.makedirs("{}/{}_{}_{}".format(self.save_path, self.joint, test_sub, test_task), exist_ok=True)
                np.savetxt("{}/{}_{}_{}/{}.csv".format(self.save_path, self.joint, test_sub, test_task, "target"), Y_te * scale)
                np.savetxt("{}/{}_{}_{}/{}.csv".format(self.save_path, self.joint, test_sub, test_task, "prediction"), Y_pred * scale)

            print('Individual Results Saved')

    def save_results(self):
        model_path = f'{self.save_path}/models'
        os.makedirs(model_path, exist_ok = True)

        gen_json = self.model.generator.to_json()
        dis_json = self.model.discriminator.to_json()

        with open(f"{model_path}/gen_{self.joint}.json", "w") as json_file:
            json_file.write(gen_json)
        with open(f"{model_path}/dis_{self.joint}.json", "w") as json_file:
            json_file.write(dis_json)
        # serialize weights to HDF5
        self.model.generator.save_weights(f"{model_path}/gen_{self.joint}.h5")
        self.model.discriminator.save_weights(f"{model_path}/dis_{self.joint}.h5")

        print("Model Saved")



        np.savetxt("{}/{}.csv".format(self.save_path, "target"), self.Y_te*self.scale)
        np.savetxt("{}/{}.csv".format(self.save_path, "prediction"), self.Y_pred*self.scale)

        print("Results Saved")
        # net.generator.save_weights(f'{checkpoint_dir}/generator.h5')



