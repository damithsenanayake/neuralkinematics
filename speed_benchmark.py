import numpy as np
from  LOWER_BODY_MASTER import LowerLimbExperiment

def powerset(s):
    x = len(s)
    masks = [1 << i for i in range(x)]
    for i in range(1 << x):
        yield [ss for mask, ss in zip(masks, s) if i & mask]

male_sub = 'Male Subject {}'
female_sub = 'Female Subject {}'
upper = False
male_subs = map(lambda x: male_sub.format(x), range(1, 16))
female_subs = map(lambda x: female_sub.format(x), range(1, 16))
joint = 'ankle'
segment_list = ['thigh', 'shank', 'foot']
task_list = ['gait05', 'gait10', 'gait15', 'gait20']
# test_tasks = ['gait05']

test_configs = list(powerset(task_list))[:-1]
print(test_configs)

#
sub_list = []
sub_list.extend(list(male_subs))
sub_list.extend(list(female_subs))

sub_string = "{} Subject {}"

# ''' Experiment Setting '''
for tc in test_configs:
    test_tasks = tc
    training_tasks = np.setdiff1d(tc)
    for tt in test_tasks:
        for i in range(1, 16):
            for s in ['Male', 'Female']:
                test_sub = sub_string.format(s, str(i))
                save_path = "thigh_shank_foot/{}_{}".format(test_sub.replace(" ", "_"), str(tt))

                train_subs = np.setdiff1d( sub_list, [test_sub])
                '''joint, all_subs, all_tasks, test_tasks, train_subs, train_tasks, segments, save_path'''
                experiment = LowerLimbExperiment(joint, sub_list, task_list, [tt], train_subs, task_list, segment_list, save_path )
                experiment.initiate()
                experiment.run_experiment()
                experiment.save_individual_results()
# for tt in test_tasks:
#     for i in range(1, 16):
#         for s in ['Male', 'Female']:
#             test_sub = sub_string.format(s, str(i))
#             save_path = "shank_foot/{}_{}".format(test_sub.replace(" ", "_"), str(tt))
#
#             train_subs = np.setdiff1d( sub_list, [test_sub])
#             '''joint, all_subs, all_tasks, test_tasks, train_subs, train_tasks, segments, save_path'''
#             experiment = LowerLimbExperiment(joint, sub_list, task_list, [tt], train_subs, task_list, segment_list[1:], save_path )
#             experiment.initiate()
#             experiment.run_experiment()
#             experiment.save_results()
#
# for tt in test_tasks:
#     for i in range(1, 16):
#         for s in ['Male', 'Female']:
#             test_sub = sub_string.format(s, str(i))
#             save_path = "foot/{}_{}".format(test_sub.replace(" ", "_"), str(tt))
#
#             train_subs = np.setdiff1d( sub_list, [test_sub])
#             '''joint, all_subs, all_tasks, test_tasks, train_subs, train_tasks, segments, save_path'''
#             experiment = LowerLimbExperiment(joint, sub_list, task_list, [tt], train_subs, task_list, segment_list[2:], save_path )
#             experiment.initiate()
#             experiment.run_experiment()
#             experiment.save_results()


#TODO: consider all possible combinations.