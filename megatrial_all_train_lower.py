import pandas as pd
import numpy as np
from util.preprocess import  resample
import matplotlib.pyplot as plt
from models.mvtsgan import MVTSGAN
from scipy.signal import butter,sosfilt
from util.visualizer import vis_imu
from util.megatrial_preprocess import read_all_for_tasks
import os
import tensorflow as tf

male_sub = 'Male Subject {}'
female_sub = 'Female Subject {}'
upper = False
male_subs = map(lambda x: male_sub.format(x), range(1, 3))
female_subs = map(lambda x: female_sub.format(x), range(1, 3))
joint = 'knee'
segment_list = ['shank', 'thigh']
task_list = ['gait05', 'gait10']
sub_list = []
sub_list.extend(list(male_subs))
sub_list.extend(list(female_subs))

X, Y = read_all_for_tasks(sub_list, task_list, ['thigh', 'foot', 'shank'], joint)
X_test, Y_test = read_all_for_tasks(sub_list, ['gait20'], ['thigh', 'foot', 'shank'], joint)

print(X.shape)
print(Y.shape)

checkpoint_path = f"/data/gpfs/projects/punim1184/neuralkinematics/mega_trial_checkpoints/{joint}"

# tr_rat = 0.8
# tr_sz = int(X.shape[0] * tr_rat)
X_tr = X
Y_tr = Y
X_te = X_test
Y_te = Y_test

fig, axes = plt.subplots(Y.shape[1], sharey=True)
net = MVTSGAN(X.shape[1], output_streams=Y.shape[1], epochs=200, lr=0.00001, batch_len=500, saturation=False, rand_noise=False,
                  verbose=1)
#
net.fit(X_tr, Y_tr)
#
Y_pr = net.generator.predict(X_te.reshape((1, X_te.shape[0], X_te.shape[1]))).reshape((Y_te.shape))
#
for i in range(Y.shape[1]):
    ax = axes.flatten()[i]
    ax.plot(Y_te.T[i][:400], label='true')
    ax.plot(Y_pr.T[i][:400], label = 'predicted')
    ax.legend()

gen_json = net.generator.to_json()
dis_json = net.discriminator.to_json()

with open(f"{checkpoint_path}/gen_{joint}.json", "w") as json_file:
    json_file.write(gen_json)
with open(f"{checkpoint_path}/dis_{joint}.json", "w") as json_file:
    json_file.write(dis_json)
# serialize weights to HDF5
net.generator.save_weights(f"{checkpoint_path}/gen_{joint}.h5")
net.discriminator.save_weights(f"{checkpoint_path}/dis_{joint}.h5")
# net.generator.save_weights(f'{checkpoint_dir}/generator.h5')
# net.discriminator.save_weights(f'{checkpoint_dir}/discriminator.h5')
plt.savefig(f'results/png/megatrial_{joint}.png')
# plt.show()
# plt.close()
