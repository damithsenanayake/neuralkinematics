from util.load_calib_data import get_calibration_imus
import numpy as np

calibration_files = ['data/Male Subject 15/IMU calibration/elbow flexion 1'] # add required calibration data folders here

segments = ['forearm', 'upperarm', 'thorax']

calib_imu = get_calibration_imus(segments, calibration_files)

print(calib_imu.shape)