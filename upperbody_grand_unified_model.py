import pandas as pd
import numpy as np
from util.preprocess import  resample
import matplotlib.pyplot as plt
from models.mvtsgan import MVTSGAN
from scipy.signal import butter,sosfilt
from util.visualizer import vis_imu
from util.megatrial_preprocess import read_all_for_tasks_upper
import os
import json
import tensorflow as tf


config_file = open('./config.json', 'rb')

configs = json.load(config_file)

print(configs)
male_sub = 'Male Subject {}'
female_sub = 'Female Subject {}'
upper = False
male_subs = map(lambda x: male_sub.format(x), range(1, 15))
female_subs = map(lambda x: female_sub.format(x), range(1, 15))
joint = 'humeral'
segment_list = ['forearm', 'upperarm', 'thorax']
task_list = [ 'head_F', 'head_S', 'reach_F', 'reach_S', 'horiFL_S', 'horiFL_F', 'FL_S', 'FL_F', 'AB_F',
              'AB_S']
sub_list = []
sub_list.extend(list(male_subs))
sub_list.extend(list(female_subs))
print(os.system(f'ls {configs["data_root"]}'))
X, Y, loc, scale = read_all_for_tasks_upper(sub_list, task_list, segment_list, joint, configs['data_root'])
X_test, Y_test, loc_t, scale_t = read_all_for_tasks_upper([male_sub.format(15)], ['FL_S'], segment_list, joint, configs['data_root'])

print(X.shape)
print(Y.shape)

checkpoint_path = f"./mega_trial_checkpoints/{joint}"

# tr_rat = 0.8
# tr_sz = int(X.shape[0] * tr_rat)
X_tr = X
Y_tr = Y
X_te = X_test
Y_te = Y_test
# Y_test += loc
# Y_test *= scale

plt.plot(Y)
plt.savefig('results/png/target.png')

fig, axes = plt.subplots(Y.shape[1], sharey=True)
net = MVTSGAN(X.shape[1], output_streams=Y.shape[1], epochs=5000, lr=0.00000001, batch_len=500, saturation=False, rand_noise=False,
                  verbose=1, chkpt_path=configs['chkpt_path'])
#

net.fit(X_tr, Y_tr)
#
Y_pr = net.generator.predict(X_te.reshape((1, X_te.shape[0], X_te.shape[1]))).reshape((Y_te.shape))
#
# Y_pr += loc_t
# Y_pr *= scale_t
for i in range(Y.shape[1]):
    ax = axes.flatten()[i]
    ax.plot(Y_te.T[i][:400], label='true')
    ax.plot(Y_pr.T[i][:400], label = 'predicted')
    ax.legend()

gen_json = net.generator.to_json()
dis_json = net.discriminator.to_json()

with open(f"{checkpoint_path}/gen_{joint}.json", "w") as json_file:
    json_file.write(gen_json)
with open(f"{checkpoint_path}/dis_{joint}.json", "w") as json_file:
    json_file.write(dis_json)
# serialize weights to HDF5
net.generator.save_weights(f"{checkpoint_path}/gen_{joint}.h5")
net.discriminator.save_weights(f"{checkpoint_path}/dis_{joint}.h5")
# net.generator.save_weights(f'{checkpoint_dir}/generator.h5')
# net.discriminator.save_weights(f'{checkpoint_dir}/discriminator.h5')
plt.savefig(f'results/png/megatrial_{joint}.png')
np.savetxt(f'{configs["chkpt_path"]}/trainedon.txt',[f'trained on {joint}'], fmt='%s')
# plt.show()
# plt.close()
