import pandas as pd
import numpy as np
from util.preprocess import  resample
import matplotlib.pyplot as plt
from models.mvtsgan import MVTSGAN
from scipy.signal import butter,sosfilt
from util.visualizer import vis_imu
from util.megatrial_preprocess import read_all_for_tasks
import os
import tensorflow as tf

male_sub = 'Male Subject {}'
female_sub = 'Female Subject {}'
upper = False
male_subs = map(lambda x: male_sub.format(x), range(1, 3))
female_subs = map(lambda x: female_sub.format(x), range(1, 3))
joint = 'knee'
segment_list = ['shank', 'thigh']
task_list = ['gait05', 'gait10']
sub_list = []
sub_list.extend(list(male_subs))
sub_list.extend(list(female_subs))

X, Y = read_all_for_tasks(sub_list, task_list, ['thigh', 'foot', 'shank'], joint)

print(X.shape)
print(Y.shape)