import os

joints = ['ankle', 'knee', 'hip']

path = 'split_corrected'

folder_format = 'lower_limb_reporting_sensor_combo_{}_{}/exp_{}'

for j in joints:
    exp = 0

    for com in range(15):
        folder_path = os.path.join(path, folder_format.format(com, j, exp))
        exp += 1
        folders = os.listdir(os.path.join(folder_path, f'{j}*'))
        print(folders)