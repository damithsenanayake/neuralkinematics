import json

import tensorflow as tf
import numpy as np
class MVTSGAN(object):
    tf.random.set_seed(10)
    def __init__(self, input_streams, output_streams = None, lr = 0.001, l2_gen = 0.0001,
                 saturation = True, epochs = 100, batch_len = 100, rand_noise = False, verbose = False, lr_reduce=True,
                 save_path = None, chkpt_path = None):
        self.input_streams = input_streams
        self.output_streams = output_streams if not output_streams == None else input_streams
        self.generator = None
        self.gen_conv = None
        self.gen_dense = None
        self.discriminator = None
        self.lr = lr
        self.optimizer = tf.keras.optimizers.legacy.Adam(lr=lr)
        self.fine_optimizer = tf.keras.optimizers.legacy.Adam(lr = lr *0.05)
        self.l2_gen = l2_gen
        self.saturate = saturation
        self.n_epochs = epochs
        self.batch_size = batch_len
        self.rand_noise = rand_noise
        self.verbose = verbose
        self.min_lr = self.lr * 0.005
        self.generator_made = False
        self.discriminator_made = False
        self.savepath = save_path
        self.chkpt_path = chkpt_path
        self.cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=True)

        start_epoch = self.get_checkpoints()

    def discriminator_loss(self, real, fake):
        real_loss = self.cross_entropy(tf.ones_like(real), real)
        fake_loss = self.cross_entropy(tf.zeros_like(fake), fake)
        total_loss = tf.reduce_mean(real_loss + fake_loss)
        return total_loss

    def generator_loss(self, fake_pred):
        return tf.reduce_mean(self.cross_entropy(tf.ones_like(fake_pred), fake_pred))

    def make_generator(self):
        if self.generator_made:
            return
        self.generator_made = True
        self.generator = tf.keras.Sequential()
        # self.gen_conv = tf.keras.Sequential()
        # self.gen_dense = tf.keras.Sequential()

        self.generator.add(tf.keras.layers.Conv1D(20, 50, padding='same', batch_input_shape=(None, None, self.input_streams)))

        self.generator.add(tf.keras.layers.Conv1D(100, 100, padding='same', activation='linear', kernel_regularizer="L2"))

        #
        #
        # self.generator.add(tf.keras.layers.Conv1D(100, 3, padding='same', activation='linear', kernel_regularizer="L2"))

        #


        # self.generator.add(tf.keras.layers.Dense(200, activation='linear'))
        # # self.generator.add(tf.keras.layers.Dropout(rate=0.8))
        #
        # self.generator.add(tf.keras.layers.Dense(20, activation='linear'))
        # self.generator.add(tf.keras.layers.Dense(20, activation='linear'))
        self.generator.add(tf.keras.layers.Dense(20, activation='linear'))
        # self.generator.add(tf.keras.layers.Dense(20, activation='linear'))
        # self.generator.add(tf.keras.layers.Dense(20, activation='linear'))
        # self.generator.add(tf.keras.layers.Dense(20, activation='linear'))
        # self.generator.add(tf.keras.layers.Dense(20, activation='linear'))

        # self.generator.add(tf.keras.layers.Dense(20, activation='linear'))
        self.generator.add(tf.keras.layers.Dense(5, activation='linear'))
        # self.generator.add(tf.keras.layers.Dropout(rate=0.8))

        self.generator.add(tf.keras.layers.Dense(self.output_streams))

        # self.gen_dense.add(tf.keras.layers.Dense(self.output_streams))
        # self.generator.add(self.gen_dense)

    def make_discriminator(self):
        if self.discriminator_made:
            return
        self.discriminator_made = True
        self.discriminator = tf.keras.Sequential()

        self.discriminator.add(tf.keras.layers.Conv1D(32, 2, batch_input_shape=(None, None, self.output_streams)))
        # self.discriminator.add(tf.keras.layers.Conv1D(100, 3, padding='same'))
        # self.discriminator.add(tf.keras.layers.MaxPool1D(2))
        self.discriminator.add(tf.keras.layers.Conv1D(32, 2))
        # self.discriminator.add(tf.keras.layers.MaxPool1D(2))

        self.discriminator.add(tf.keras.layers.Conv1D(32, 2, padding='same', activation='linear'))
        # self.discriminator.add(tf.keras.layers.MaxPool1D(2))

        self.discriminator.add(tf.keras.layers.Dense(500, activation='linear'))
        self.discriminator.add(tf.keras.layers.Dense(20, activation='tanh'))
        self.discriminator.add(tf.keras.layers.Dense(10, activation='linear'))
        self.discriminator.add(tf.keras.layers.Dense(5, activation='linear'))

        self.discriminator.add(tf.keras.layers.Dense(1, activation='sigmoid'))

    @tf.function
    def train_step(self, real_input, real_target):

        with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:

            if self.rand_noise:
                real_input += tf.random.normal(real_input.shape, stddev=1.)

            reconstructed = self.generator(real_input)
            fake_out = (self.discriminator(reconstructed))
            real_out = (self.discriminator(real_target))

            discriminator_loss = -tf.reduce_mean(tf.math.log(real_out) + tf.math.log(1 - fake_out))

            if self.saturate:
                generator_loss = tf.reduce_mean(1 - tf.math.log(fake_out))# + self.generator.losses
            else:
                generator_loss = -tf.reduce_mean(tf.math.log(fake_out))

            generator_loss += tf.reduce_mean((tf.keras.losses.MSE(real_target, reconstructed) + tf.keras.losses.MAE(real_target, reconstructed) + tf.keras.losses.mean_squared_logarithmic_error(real_target, reconstructed)) )

        gen_gradient = gen_tape.gradient(generator_loss, self.generator.trainable_variables)
        dis_gradient = disc_tape.gradient(discriminator_loss, self.discriminator.trainable_variables)

        self.optimizer_gen.apply_gradients(zip(gen_gradient, self.generator.trainable_variables))
        self.optimizer_dis.apply_gradients(zip(dis_gradient, self.discriminator.trainable_variables))
        # rmse = tf.keras.metrics.RootMeanSquaredError()
        metric = self.rmse(real_target, reconstructed)
        #
        return generator_loss, discriminator_loss, metric

    def get_checkpoints(self):
        print(f'{"#" * 50}')
        print(f' Attempting to load check points from {self.chkpt_path}')

        try:
            gen_model = json.dumps(json.load(open(f'{self.chkpt_path}/gen.json', 'rb')))
            dis_model = json.dumps(json.load(open(f'{self.chkpt_path}/dis.json', 'rb')))
            self.generator = tf.keras.models.model_from_json(gen_model)
            self.generator.load_weights(f'{self.chkpt_path}/gen_w.h5')
            self.discriminator = tf.keras.models.model_from_json(dis_model)
            self.discriminator.load_weights(f'{self.chkpt_path}/dis_w.h5')
            print('discriminator and generator loaded from checkpoint')
            epochs = np.loadtxt(f'{self.chkpt_path}/epochs.txt')
            print(f' starting from {epochs} epochs')
            return int(epochs)

        except:
            print(f'checkpoint path invalid {self.chkpt_path}')
            return 0


    def fit(self, input_signal, target_signal, pick_rate = 1.0):
        self.make_generator()
        self.make_discriminator()

        start_epoch = self.get_checkpoints()

        batch_size = self.batch_size

        n_batches = input_signal.shape[0] // batch_size

        print(f'batch_size = {batch_size} \n n_batches = {n_batches}')

        g_losses = []
        d_losses = []

        self.lr_schedule_gen = tf.keras.optimizers.schedules.ExponentialDecay(
            initial_learning_rate=self.lr,
            decay_steps=10000,
            decay_rate=0.9)
        self.optimizer_gen = tf.keras.optimizers.Adam(learning_rate=self.lr)

        self.lr_schedule_dis = tf.keras.optimizers.schedules.ExponentialDecay(
            initial_learning_rate=self.lr,
            decay_steps=10000,
            decay_rate=0.9)
        self.optimizer_dis = tf.keras.optimizers.Adam(learning_rate=self.lr)

        for i in range(start_epoch, self.n_epochs):
            # lr = finish_lr + (starting_lr - finish_lr) * (1-i * 1./self.n_epochs)


            gen_loss = 0
            dis_loss = 0
            metric = 0
            skip_rate = 1-pick_rate#0.95
            self.rmse = tf.keras.metrics.RootMeanSquaredError()

            n_train_batches = int(n_batches * pick_rate)
            input_batch_ixs = np.random.choice(np.arange(n_batches), n_train_batches, replace=False)

            selected_indexes = []

            for b in input_batch_ixs:
                selected_indexes.extend(list(range(b*batch_size, min((b+1) * batch_size, input_signal.shape[0]))))

            batch_input = input_signal[selected_indexes].reshape((n_train_batches, self.batch_size, self.input_streams))#[input_signal[b*batch_size:min((b+1) * batch_size, input_signal.shape[0])] for b in input_batch_ixs]
            batch_target = target_signal[selected_indexes].reshape((n_train_batches, self.batch_size, self.output_streams))#[target_signal[b*batch_size:min((b+1) * batch_size, input_signal.shape[0])] for b in input_batch_ixs]
            g,d,m = self.train_step(batch_input, batch_target)

            # for b in np.random.permutation(range(n_batches)):
            #
            #     if np.random.binomial(1, skip_rate):
            #         continue
            #
            #     b_st = b * batch_size
            #     b_en = min((b+1) * batch_size, input_signal.shape[0])
            #
            #     g, d, m = self.train_step(input_signal[b_st:b_en].reshape(1, b_en - b_st, self.input_streams), target_signal[b_st:b_en].reshape(1, b_en - b_st, self.output_streams))
            #
            #     if np.isnan(g).any() or np.isnan(d).any():
            #         return g_losses, d_losses
            gen_loss += g
            dis_loss += d
            metric += m
            # g_losses.append(np.mean(gen_loss))
            # d_losses.append(dis_loss)

            if not i%20:
                ''' Save Checkpoints '''
                with open(f"{self.chkpt_path}/gen.json", "w") as json_file:
                    json_file.write(self.generator.to_json())
                with open(f"{self.chkpt_path}/dis.json", "w") as json_file:
                    json_file.write(self.discriminator.to_json())
                # serialize weights to HDF5
                self.generator.save_weights(f"{self.chkpt_path}/gen_w.h5")
                self.discriminator.save_weights(f"{self.chkpt_path}/dis_w.h5")

                np.savetxt(f'{self.chkpt_path}/epochs.txt',[i])


            if self.verbose and not i %20:
                print('training epoch {}: g loss = {}: d loss = {}: RMSE = {} learning rate = {}'.format(i+1, gen_loss, dis_loss, metric, self.optimizer_gen.learning_rate.numpy()))
            # if not (self.savepath == None):
            #     self.generator.save(f'{self.savepath}/generator.h5')
            #     self.discriminator.save(f'{self.savepath}/discriminator.h5')

        with open(f"{self.chkpt_path}/gen_fin.json", "w") as json_file:
            json_file.write(self.generator.to_json())
        with open(f"{self.chkpt_path}/dis_fin.json", "w") as json_file:
            json_file.write(self.discriminator.to_json())
        # serialize weights to HDF5
        self.generator.save_weights(f"{self.chkpt_path}/gen_w_fin.h5")
        self.discriminator.save_weights(f"{self.chkpt_path}/dis_w_fin.h5")

        return g_losses, d_losses