!/bin/bash

#PBS -l ncpus=16
#PBS -l ngpus=8
#PBS -l mem=190GB
#PBS -l jobfs=200GB
#PBS -q normal
#PBS -P ph79
#PBS -l walltime=48:00:00
#PBS -l storage=gdata/ph79+scratch/ph79
#PBS -l wd

module load python3/3.9.2 tensorflow/2.6.0
python3 ~/neuralkinematics/upperbody_grand_unified_model.py > /home/563/ds0871/neuralkinematics/unified.out
python3 ~/neuralkinematics/load_test_upper.py > ~/neuralkinematics/testing_loading.out

