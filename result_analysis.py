import numpy as np
from sklearn.metrics import r2_score
# import spm1d as sp
import os
import glob

import matplotlib.pyplot as plt

def RMSE(x, y):
    res = x-y
    res **=2
    res = res.sum(axis=0)
    res /= np.float(len(res))
    res **= 0.5

    return list(res)

def calculate_stats(file_path):
    try:
        target = np.loadtxt(f'{file_path}/target.csv')
        predic = np.loadtxt(f'{file_path}/prediction.csv')
    except FileNotFoundError:
        raise Exception()
    print(target.shape)
    print(predic.shape)
    rmse = RMSE(target, predic)
    r2s = []
    for i in range(target.shape[1]):
        r2s.append(r2_score(target[:, i], predic[:, i])) #= r2_score(target, predic)
    return rmse, r2s


def listdirs(rootdir):
    list_of_dirs = []
    for it in os.scandir(rootdir):
        if it.is_dir():
            list_of_dirs.append(it.path)

    return list_of_dirs
            # listdirs(it)

def plot_results(data_path):
    target = np.loadtxt(f'{data_path}/target.csv')
    predic = np.loadtxt(f'{data_path}/prediction.csv')
    fig, axes = plt.subplots(target.shape[1], sharey=True)

    for i in range(target.shape[1]):
        ax = axes.flatten()[i]
        ax.plot(target.T[i][:400], label='true')
        ax.plot(predic.T[i][:400], label='predicted')
        ax.legend()

    plt.savefig(f'{data_path}/vis.png')


def iterate_through_folders(root_path):
    dir_list = listdirs(root_path)

    table = []

    for directory in dir_list:
        entry = []
        entry.append(directory.split('/')[-1])
        plot_results(directory)
        try:
            rmse, r2 = calculate_stats(directory)
            entry.extend(rmse)
            entry.extend(r2)

            print(entry)
            table.append(entry)
        except:
            continue

    return table


table = iterate_through_folders('CompoundExperiments')

np.savetxt('cross_sub_results/RES_TABLE.csv', table, fmt='%s')
